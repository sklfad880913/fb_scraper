#crawl ajax webpage: https://www.youtube.com/watch?v=IMOUf4BYTG8 

# In[0]: 資料庫

import pymysql
# 資料庫參數設定
db_settings = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "password": "[db_pwd]",
    "db": "fb_scraper",
    "charset": ""
}

# In[1]: 爬所有頁面中的貼文連結

from bs4 import BeautifulSoup
import requests
import json
import re
import time

def remove_space(string):
    j = 5
    while(j > 0):
        string = re.sub('\n\n', '\n', string)
        string = re.sub('\t\t', '\t', string)
        string = re.sub('  ', '', string)
        j -= 1
    return string

page = 1
links = []   #存放貼文連結
#F12觀察向哪個網站連線取得資料
while(True):
    print("page= ", page)
    r = requests.get("https://www.yourator.co/api/v2/jobs?position[]=intern&page="+str(page))   #只有抓一頁
    data = json.loads(r.text)   #將 JSON 資料解析成字典/列表的格式
    posts = data["jobs"]    #若爬的頁數已超過所有頁數，則posts[] = 0
    if(len(posts) == 0):   #爬完所有頁數了
        break
    page += 1
    
    
    for i in posts:
        links.append(i["path"])

# In[2]: 爬每篇貼文的內容

header = "https://www.yourator.co"    
for i in links:    #爬每篇貼文
    #print(i)
    r_post = requests.get(header+i)
    
    soup = BeautifulSoup(r_post.text,"html.parser")
    
    #抓貼文欄位
    blank_bs4 = soup.find_all(class_="job-heading")
    blank = []   #存放貼文欄位名
    time.sleep(1)    
    
    #抓貼文內容
    content_bs4 = soup.find_all(class_="content__area")
    content = []  #存放貼文欄位對應資料
        
    #抓貼文標題、公司名稱
    title_bs4 = soup.select("div.basic-info__title.flex.flex-align-center")
    title = remove_space(title_bs4[0].text)
    cp_name = remove_space(title_bs4[1].text)
    blank.append("貼文標題")
    content.append(title)
    blank.append("公司名稱")
    content.append(cp_name)
        
    for j in range(len(blank_bs4)):
        blank.append(blank_bs4[j].text)
        content_temp = remove_space(content_bs4[j].text)
        content.append(content_temp)
    
    #抓工作地點
    location_bs4 = soup.select("div.basic-info__icon.basic-info__icon--location a")
    if(len(location_bs4) == 1):
        location = remove_space(location_bs4[0].text)
    else:
        location = remove_space(location_bs4[1].text)
    blank.append("地點")
    content.append(location)
    
    if(blank[4] != "遠端型態"):
        remote_status = ""
    else:
        remote_status = content[4]

    try:
        conn = pymysql.connect(**db_settings)
        with conn.cursor() as cursor:
            insert = "INSERT INTO yourator_post_info(link, title, cp_name, job_desc, requirement, remote_status, benefits, salary, location) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(insert, (i ,content[0], content[1], content[2], content[3], remote_status, content[-3], content[-2], location))
            conn.commit()
    except Exception as ex:
        print(ex)
        continue














