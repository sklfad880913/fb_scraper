[此為大學畢業專題Intern Sandwich實習平台之爬蟲code]

我爬了104人力銀行、FB社團_實習透視鏡、Yourator新創職涯平台三大平台的實習資訊，並將資料前處理後存入資料庫

主要用Selenium套件爬蟲，爬取FB需要裝chromedriver模擬使用者行為，點擊網頁上「查看更多貼文」按鈕(chromedriver須留意版本是否符合瀏覽器版本)




- 爬取網頁note:
1. FB社團屬於靜態網頁，不過他的HTML Tree架構較複雜，因此我爬取FB手機版網頁
2. 104人力銀行在爬取貼文詳細頁面時，需要標明來源頁面網址(Referer參數)，否則無法爬取成功
3. Yourator新創職涯平台屬於動態生成網頁，使用AJAX技術向遠端server動態請求資料，因此需要找出真正請求頁面的網址才可爬取成功


- 資料處理&儲存note:
1. 由於FB貼文資料格式不一，因此我先在fb_content.py爬取每篇貼文連結，貼文詳細內容爬取與資料處理部分我移至data_processing.py
2. 104人力銀行及Yourator因為都有規定好發文格式，因此從爬取、資料處理至儲存我都寫在同一份code，分別是104_content.py、yourator_content.py
