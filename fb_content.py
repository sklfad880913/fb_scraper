# In[1]:
#函式:

#點擊文字為text的button
def click_more(driver, text):
    driver.find_element_by_link_text(text).click() 
    """
    for i in range(1):
        comment_more = driver.find_elements_by_partial_link_text(text)
        if len(comment_more) == 0:
            break
        for btn in comment_more:
            try:
                btn.click()
                sleep(1)
                print('. ',end='')
            except:
                continue
    """
#檢查是否為同篇文章(主要避免重複爬到版規；用於單次爬文)
def check_same_post(post, txt):
    same = False
    for i in post:
        if(i[0:30] == txt[0:30]):
            same = True
            break
    return same

# In[2]:
#主要爬蟲部分:

import selenium
from selenium import webdriver
from bs4 import BeautifulSoup
import numpy as np
import time
import csv
import re


chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)
driver=webdriver.Chrome("C:/Users/user/Desktop/專題/chromedriver.exe",chrome_options=chrome_options)

#driver.get("http://www.facebook.com")
#time.sleep(3)

#driver.find_element_by_id("email").send_keys("sklfad880913@gmail.com") # 將USERNAME改為你的臉書帳號
#driver.find_element_by_id("pass").send_keys("") # 將PASSWORD改為你的臉書密碼
#driver.find_element_by_id("u_0_d_NY").click() #登入
#time.sleep(3)
club_url = 'http://mbasic.facebook.com/groups/internlens'
driver.get(club_url) #社團網站

post=[]
first_arr=[]
latest_page_links=[]

for j in range(0,2):
    soup=BeautifulSoup(driver.page_source,"lxml")
    first=soup.find_all(class_="bx")        
    first_arr.append(first)
    click_more(driver, '查看更多貼文')

    
for k in first_arr:    #每一頁  
    #if(k == first_arr[0]): 
        #continue
    for i in k:    #每一頁的每則貼文
        same = check_same_post(post, i.text)
        if(same):
            continue
        if(i.text.find("更多") != -1):
            x = '<a href="https://m.facebook.com/groups' #re.search(pattern, str(i)).group(0)
            start_index = str(i).find(x)+9
            end_index = str(i).find("\">更多")
            more_link = str(i)[start_index:end_index]    #全文網址(點擊「更多」後的頁面)
            if(more_link[0:5] != "https"):   #確保是否為網址
                continue
            driver.get(more_link)
            s = more_link.find("permalink/")+10
            e = more_link.find("/", s)
            more_link = more_link[s:e]
            more_soup = BeautifulSoup(driver.page_source, 'lxml')
            more_first = more_soup.find(class_="_5rgt _5nk5")
            if(more_first.text == ""):    #文章不得為空
                continue
            post.append(more_first.text)    #全文

        #貼文沒有長到需要點擊更多才能看到全文
        else:
            if(i.text == ""):   #文章不得為空
                continue
            post.append(i.text)
            more_link = ""
        
        #將第一頁(最新那頁)爬到的文章連結存入lastest_page_links[] -> 用於爬蟲斷點
        #if(k == first_arr[0]):         
        latest_page_links.append(more_link)
        


# In[14]:
#把每篇全文存入excel檔

import pandas as pd
df = pd.DataFrame(post, columns=['content'])
df.to_excel("fb_post_content.xlsx", index=False)


# In[15]:
#存入資料庫

import pymysql
# 資料庫參數設定
db_settings = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "password": "[db_pwd]",
    "db": "fb_scraper",
    "charset": ""
}
try:
    # 建立Connection物件
    conn = pymysql.connect(**db_settings)
    
    # 建立Cursor物件
    with conn.cursor() as cursor:
        insert = "INSERT INTO post(link) VALUES(%s)"
        select = "SELECT * FROM post WHERE link = %s"
        check_duplicate = False
        print("success!")
        for l in latest_page_links:
            cursor.execute(select, (l))
            results = cursor.fetchall()
            if(len(results) != 0):
                #if(check_duplicate):    #重複文*2就不繼續存了
                    #break
                #check_duplicate = True
                continue
            else:
                print(l)
                cursor.execute(insert, l)
                #check_duplicate = False
        """
        link = latest_page_links[0][:51]+results[1][0]
        driver.get(link)
        more_soup = BeautifulSoup(driver.page_source, 'lxml')
        more_first = more_soup.find(class_="_5rgt _5nk5")
        """

        """
        for result in results:
            print("餐廳名: ", result[0], "\t價位: ", result[1])
        """
        conn.commit()
except Exception as ex:
    print(ex)




# In[16]:


#post需要的點擊

#click_more(driver, '顯示先前')
#click_more(driver, '檢視另')
#click_more(driver, '查看另')
#click_more(driver, '已回覆')
#click_more(driver, '查看更多貼文')


