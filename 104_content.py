# 爬蟲js render: https://ithelp.ithome.com.tw/questions/10198403
# https://ithelp.ithome.com.tw/questions/10195198

# In[0]: 資料庫

import pymysql
# 資料庫參數設定
db_settings = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "password": "[db_pwd]",
    "db": "fb_scraper",
    "charset": ""
}

# In[1]: 爬所有頁面中的貼文連結

from bs4 import BeautifulSoup
import requests
import json

#避免最後一個字是頓號
def comma_filter(s):
    if(s[-1] == "、"):
        s = s[:-1]
    return s

page = 2
links = []   #存放貼文連結
require_name = {"workExp": "工作經歷", "edu": "學歷要求", "major": "科系要求", "language": "語文條件", "specialty": "擅長工具", 
"skill": "工作技能", "certificate": "具備證照", "driverLicense": "具備駕照", "other": "其他條件"}
contact_name = {"hrName": "聯絡人", "email": "E-mail", "phone": "電洽", "other": "其他",
"reply": "應徵回覆"}

while(page < 4):
    url = "https://www.104.com.tw/jobs/search/?ro=0&kwop=7&keyword=%E5%AF%A6%E7%BF%92%20intern&order=1&asc=0&rostatus=1024&page="+str(page)+"&mode=s&jobsource=indexpoc2018"
    r = requests.get(url)
    soup=BeautifulSoup(r.text,"lxml")
    first = soup.find_all(class_="js-job-link")
    page += 1
    for i in first:
        links.append(i["href"])   #取得每篇連結

#爬每一篇內容
for i in links: 
    ind_start = i.find("job/")
    ind_end = i.find("?")
    ajax = "https:"+ i[:ind_start+4] + "ajax/content/" + i[ind_start+4:ind_end]
    headers = {
        "Referer": ("https:"+i),
    }
    r_content = requests.get(url = ajax, headers = headers)
    data = json.loads(r_content.text)   #data為送request回傳的內容
    title = data["data"]["header"]["jobName"]
    cp_name = data["data"]["header"]["custName"]
    job_desc = data["data"]["jobDetail"]["jobDescription"]
    link = "https:"+i[:ind_end]
    print("link= https:"+i[:ind_end])
    #print(title)
    #print(cp_name)
    #print(job_desc)

    #條件要求
    requirement = ""
    for d in data["data"]["condition"]:
        if(d == "acceptRole" or (d == "localLanguage")):
            continue
        if(data["data"]["condition"][d] == [] or (data["data"]["condition"][d] == '')):   #沒有value的key
            continue
        #print(require_name[d] +": ",end='')
        requirement += require_name[d]+": "   #條件要求欄位名

        #其他條件
        if(d == "other"):
            requirement += "\n"

        #擅長工具
        if(d == "specialty"):
            for j in data["data"]["condition"]["specialty"]:
                requirement += (j["description"]+"、")
                #print(j["description"])

        #科系要求
        elif(d == "major"):
            for j in data["data"]["condition"]["major"]:
                requirement += (j + "、") 
                #print(j)

        #語言條件
        elif(d == "language"):
            for k in data["data"]["condition"]["language"]:
                requirement += (k['language']+": "+k["ability"])
                #print(k['language']+": "+k["ability"])
        
        #具備駕照
        elif(d == "driverLicense"):
            for k in data["data"]["condition"][d]:
                requirement += (k + "、")
        
        #工作技能
        elif(d == "skill"):
            for k in data["data"]["condition"][d]:
                requirement += (k["description"] + "、")
        else:
            requirement += data["data"]["condition"][d]
            #print(data["data"]["condition"][i])
        requirement = comma_filter(requirement)
        requirement += "\n"
    #print(requirement)

    #待遇福利
    benefits = data["data"]["jobDetail"]["salary"]
    if(data["data"]["welfare"]["legalTag"] != []):
        benefits += "\n法定項目: "
        for b in data["data"]["welfare"]["legalTag"]:
            benefits += (b+"、")
        benefits = comma_filter(benefits)
    if(data["data"]["welfare"]["welfare"] != ''):
        benefits += ("\n其他福利:\n" + data["data"]["welfare"]["welfare"])
    #print(benefits)

    #地址
    location = (data["data"]["jobDetail"]["addressRegion"]+data["data"]["jobDetail"]["addressDetail"])
    #print(location)

    #更新日期
    date = data["data"]["header"]["appearDate"]
    #print(date)

    #其他(聯絡資訊)
    other = "聯絡資訊: "
    for c in data["data"]["contact"]:
        if(c == "suggestExam" or (c == "visit")):
            continue
        if(data["data"]["contact"][c] == ''):
            continue
        if(c == "other"):
            #print(contact_name[c]+": ")
            other += ("\n"+contact_name[c]+": \n")
        else:
            #print(contact_name[c]+": ", end='')
            other += ("\n"+contact_name[c]+": ")
        #print(data["data"]["contact"][c])
        other += data["data"]["contact"][c]
    #print(other)


    #存入db
    try:
        conn = pymysql.connect(**db_settings)
        with conn.cursor() as cursor:
            insert = "INSERT INTO update_104(link, title, cp_name, job_desc, requirement, benefits, location, others, source) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(insert, (link ,title, cp_name, job_desc, requirement, benefits, location, other, 2))
            conn.commit()
    except Exception as ex:
        print(ex)
        continue
    
    
    
    
    
    
    
    
    
