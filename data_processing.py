# In[1]:
#從資料庫提取資料
links = []

import pymysql
# 資料庫參數設定
db_settings = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "password": "[db_pwd]",
    "db": "fb_scraper",
    "charset": ""
}
try:
    # 建立Connection物件
    conn = pymysql.connect(**db_settings)
    
    # 建立Cursor物件
    with conn.cursor() as cursor:
        select = "SELECT * FROM post"
        cursor.execute(select)
        results = cursor.fetchall()
        
        head = "https://m.facebook.com/groups/internlens/permalink/"
        for result in results:
            if(result[0] != ''):
                links.append(head+result[0]+"/")
                
        conn.commit()
except Exception as ex:
    print(ex)

# In[2]:

#過濾掉html tag，留下文字
import re
def filter_html_tag(string):
    string = re.sub('<.+.>', '\n', string)
    string = re.sub('<.>', '\n', string)
    i = 5
    while(i > 0):
        string = re.sub(' ', '', string)
        string = re.sub('\n\n', '\n', string)
        i -= 1
    return string


#貼文資料處理(尚未處理無【】的貼文)&英文內容&重複字詞ex.links[30]"薪資"
from bs4 import BeautifulSoup
def data_processing(post_link):
    post_info = []   #儲存貼文欄位資訊:
    #連結失效例外處理
    try:  
        driver.get(post_link)
        more_soup = BeautifulSoup(driver.page_source, 'lxml')
        more_first = more_soup.find(class_="_5rgt _5nk5")
        plaintxt = more_first.text
        more_first = more_first.prettify()
        
        #re.search("<.+.>\n", more_first.prettify())
        #公司名稱
        #需再處理將公司名寫在標題的狀況
        start_ind = plaintxt.find("【公司名稱】")
        if(start_ind == -1):
            #print("無公司名")
            return
        else:
            end_ind = plaintxt.find("【", start_ind+1)
            cp_name = plaintxt[start_ind+6:end_ind]
            #print(cp_name, "\n")
            post_info.append(cp_name)
            
    
        #應徵條件 or 資格: 需處理加分條件
        #可能包含加分條件
        combine_require = ""    #基本條件+加分條件
        start_ind = more_first.find("條件")
        if(more_first[start_ind-2:start_ind] == "加分" or start_ind == -1):
            start_ind = more_first.find("資格")
        if(start_ind == -1):
            print("") #無應徵條件")
        else:
            start_ind = more_first.find("】",start_ind)
            end_ind = more_first.find("【", start_ind+1)
            require = more_first[start_ind+1: end_ind]
            require = filter_html_tag(require)
            #print(require, "\n")
            combine_require = require
        
        #加分條件
        start_ind = more_first.find("【加分條件】")
        if(start_ind == -1):
            print("") #無加分條件")
        else:
            end_ind = more_first.find("【", start_ind+6)
            plus = more_first[start_ind+6: end_ind]
            plus = filter_html_tag(plus)
            #print(plus, "\n")
            combine_require += "\n加分條件:\n"+plus
        
        post_info.append(combine_require)
    
        #薪資待遇
        #資訊可能包含勞健保
        combine_benefits = ""   #薪資+待遇+有無勞健保
        start_a = more_first.find("【薪資")
        start_b = more_first.find("【待遇")
        start_ind = max(start_a, start_b)
        if(start_ind == -1):
            print("") #無薪資待遇")
        else:
            start_ind = more_first.find("】",start_ind)
            end_ind = more_first.find("【", start_ind+1)
            salary = more_first[start_ind+1: end_ind]
            salary = filter_html_tag(salary)
            #print(salary, "\n")
            combine_benefits += "薪資: " + salary
        
        #勞健保
        start_ind = more_first.find("健保】")
        if(start_ind == -1):
            print("") #無勞健保")
        else:
            end_ind = more_first.find("【", start_ind+1)
            insurance = more_first[start_ind+3:end_ind]
            insurance = filter_html_tag(insurance)
            #print(insurance, "\n")
            combine_benefits += "\n有無勞健保: " + insurance
        
        #公司福利
        start_ind = more_first.find("【公司福利】")
        if(start_ind == -1):  
            print("") #無公司福利")
        else:
            end_ind = more_first.find("【", start_ind+1)
            benefits = more_first[start_ind+6:end_ind]
            benefits = filter_html_tag(benefits)
            #print(benefits, "\n")
            combine_benefits += "\n公司福利: \n" + benefits
        
        post_info.append(combine_benefits)
        
        #工作內容: 
        start_ind = more_first.find("內容】")
        if(start_ind == -1):
            #print("無工作內容")
            post_info.append("")
            #若無應徵條件也無工作內容，則視貼文資訊不合格
            if(require == ""):   
                return
        else:
            #start_ind = more_first.find("】",start_ind)
            end_ind = more_first.find("【", start_ind+1)
            job_desc = more_first[start_ind+3:end_ind]
            job_desc = filter_html_tag(job_desc)
            #print(job_desc, "\n")
            post_info.append(job_desc)
    
        #工作時間
        start_ind = more_first.find("時間】")
        if(start_ind == -1):
            start_ind = more_first.find("期間】")
            if(start_ind == -1):
                #print("無工作時間")
                post_info.append("")
        else:
            end_ind = more_first.find("【", start_ind+1)
            time = more_first[start_ind+3:end_ind]
            time = filter_html_tag(time)
            #print(time, "\n")
            post_info.append(time)
    
        #工作地點
        start_ind = plaintxt.find("地點】")
        if(start_ind == -1):
            #print("無地點")
            post_info.append("")
        else:
            sub_start = more_first.find(plaintxt[start_ind+3:start_ind+7])
            if(sub_start == -1 and not '\u4e00' <= plaintxt[start_ind+3:start_ind+4] <= '\u9fa5'):  #地址第一個字元非中文
                sub_start = more_first.find(plaintxt[start_ind+4:start_ind+8])
            sub_end = more_first.find("\n", sub_start)
            location = more_first[sub_start:sub_end]
            #print(location, "\n")
            post_info.append(location)
        
        #聯絡方式
        start_a = more_first.find("【聯絡方式】")
        start_b = more_first.find("【連絡方式】")
        start_c = more_first.find("【聯繫方式】")
        start_ind = max(start_a, start_b, start_c)
        if(start_ind == -1):
            #print("無聯絡方式")
            post_info.append("")
        else:
            end_ind = more_first.find("【", start_ind+1)
            contact = more_first[start_ind+6:end_ind]
            if(end_ind == -1):
                contact = more_first[start_ind+6:]
            contact = filter_html_tag(contact)
            #print(contact, "\n")
            post_info.append(contact)
    
    except AttributeError:
        #將失效連結從資料表中移除
        conn = pymysql.connect(**db_settings)
        with conn.cursor() as cursor:
            delete = "delete from post where link = %s"
            cursor.execute(delete, post_link[51:67])
            conn.commit()
        print("錯誤: 連結失效")
    
    return post_info

#iter = re.finditer("<.+.>", pause_desc)  #recursive
#s_i = [m.start() for m in iter]
#iter = re.finditer("<.+.>", pause_desc)
#e_i = [m.end() for m in iter]
#偉哉語法: pause_desc = re.sub('<.+.>', '\n', pause_desc)
    
# In[3]:
#main part
import selenium
from selenium import webdriver

chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)
driver=webdriver.Chrome("C:/Users/user/Desktop/專題/chromedriver.exe",chrome_options=chrome_options)

for i in links:
    post_info = []   #公司名、徵求條件、薪資待遇勞健保、工作內容、工作時間、地點、聯絡方式
    ret = data_processing(i)
    #缺少如公司名稱、條件與工作內容等必要項
    if(ret == None):   
        print("不合格的貼文\n")
    else:
        post_info = ret
        #把每篇貼文資訊按欄位存進資料庫裡(db: post_info2)
        try:
            conn = pymysql.connect(**db_settings)
            with conn.cursor() as cursor:
                insert = "INSERT INTO post_info2(link, cp_name, requirement, benefits, job_desc, time, location, contact) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(insert, (i[51:67] ,ret[0], ret[1], ret[2], ret[3], ret[4], ret[5], ret[6]))
                conn.commit()
        except Exception as ex:
            print(ex)
            continue
